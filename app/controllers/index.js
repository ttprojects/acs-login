$.btnLogin.addEventListener('click', function(e) {

	if ($.tfEmail.value == '') {
		alert('Please enter a valid email address');
		return;
	}

	if ($.tfPassword.value == '') {
		alert('Please enter password');
		return;
	}

	/*
	 * Test User Email: test@test.com
	 * Username: test
	 * Password : 1234
	 */

	Alloy.Globals.Cloud.Users.login({
		login : $.tfEmail.value,
		password : $.tfPassword.value
	}, function(e) {
		if (e.success) {
			Ti.API.info("Logged in user, id = " + e.users[0].id + ", session ID = " + Alloy.Globals.Cloud.sessionId);
			var winSecret1 = Alloy.createController('Secret1', {}).getView();

			Alloy.Globals.navGroup = Titanium.UI.iOS.createNavigationWindow();
			Alloy.Globals.navGroup.window = winSecret1;
			Alloy.Globals.navGroup.open();
		} else {
			alert("Login failed.");
		}
	});

});

$.win.open();
