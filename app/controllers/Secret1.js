var args = arguments[0] || {};

var btnLogtout = Ti.UI.createButton({
	// systemButton : Ti.UI.iPhone.SystemButton.ACTION
	systemButton : Ti.UI.iPhone.SystemButton.STOP
});

btnLogtout.addEventListener('click', function() {
	Alloy.Globals.Cloud.Users.logout(function(e) {
		if (e.success) {
			alert('Success: Logged out');
			Alloy.Globals.navGroup.close();
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
});

$.lblTitle.addEventListener('click', function(e) {
	var winSecret2 = Alloy.createController('Secret2', {}).getView();
	Alloy.Globals.navGroup.openWindow(winSecret2);
});

$.winStart.setRightNavButton(btnLogtout);
